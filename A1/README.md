> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Thomas Martin

### Assignment 1 Requirements:
>
>
> #### Git commands w/short descriptions:

1. git init - to initialise a git repository for a new or existing project
2. git status - to check the status of files you've changed in your working directory
3. git add - adds changes to stage/index in your working directory
4. git commit - commits your changes and sets it to new commit object for your remote
5. git push - push your changes to remote directory
7. git config - to set your user name and email in the main configuration file

#### Assignment Screenshots:

*Screenshot of AMPPS running http://localhost*:

![AMPPS Installation Screenshot](img/ampps.png)

*Screenshot of running ERD*:

![Assignment 1 ERD Screenshot](img/a1erd.png)


#### Tutorial Links:

[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/tommywmartin/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")