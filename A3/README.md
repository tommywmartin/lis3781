> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS 3781 - Advanced Database Management

## Thomas Martin

### Assignment 3 Requirements:


#### Assignment Screenshots:

*Screenshot of SQL Code*:

![A3 SQL Code Screenshot](img/sqlcode.png)

*Screenshot of SQL Code*:

![A3 SQL Code Screenshot](img/sqlcode2.png)

*Screenshot of SQL Populated Tables*:

![SQL Populated Tables Screenshot](img/populatedtables.png)


#### Tutorial Links:

*Bitbucket LIS3781 Repo - Station Locations:*
[A3 Bitbucket LIS3781 Repo](https://bitbucket.org/tommywmartin/lis3781/src/master/ "LIS3781 Repo")
