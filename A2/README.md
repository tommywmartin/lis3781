> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS3781 - Advanced Database Management

## Thomas Martin

### Assignment 2 Requirements:


#### Assignment Screenshots:

*Screenshot of SQL Code*:

![SQL Code Screenshot](img/a2screenshot1.png)

*Screenshot of SQL Code*:

![SQL Code Screenshot](img/a2screenshot2.png)

*Screenshot of SQL Code*:

![SQL Code Screenshot](img/a2screenshot3.png)

*Screenshot of Populated Tables*:

![Populated Tables](img/a2screenshot4.png)


#### Tutorial Links:

[A2 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/tommywmartin/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

[A2 Bitbucket LIS3781 Location](https://bitbucket.org/tommywmartin/lis3781/src/master/ "Bitbucket Folder Locations")